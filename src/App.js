import Main from './components/main';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { configureStore } from './redux/configureStore';
import './App.css';

function App() {
  const store = configureStore();
  return (
    <Provider store = {store}>
      <BrowserRouter>
        <div className="App">
          <Main /> {/* main functional component */}
        </div>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
