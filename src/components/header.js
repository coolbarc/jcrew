import { useState } from "react";
import {Navbar, NavbarBrand, Nav, NavbarToggler, Collapse, NavItem, Container, inNavbar, divider, Input,
  NavLink, Button, DropdownMenu, UncontrolledDropdown, DropdownToggle, caret, DropdownItem} from 'reactstrap';


  const Header = props => {
    
    const [open, setOpen]=useState(false);{/*  open collapse */}
    const [present, setPresent]=useState(false); 
    return(<>
      <div className='global-top'>
            JUST FOR J.CREW REWARDS MEMBERS: GET DOUBLE POINTS ON CASHMERE *
      </div>
      {/* Begining Navbar */}
      <Navbar color='light' light expand='lg'>
        <div>
          <NavbarToggler onClick={()=>setOpen(!open)} ></NavbarToggler>
           {open === true?<span className='m-4 fa fa-search fa-lg'></span> : null}
        </div>
        <NavbarBrand ><h1>J.CREW</h1></NavbarBrand>
        <Collapse isOpen={open} navbar>
            <Nav className='mr-auto' navbar>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                PRODUCTS
              </DropdownToggle>
              <DropdownMenu left>
                <DropdownItem href='/products'>
                  products
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem>
                  Reset
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            
            </Nav>
        </Collapse>
        <Nav navbar>
          <NavItem>
            <Input placeholder='&#xf002; search' onClick='' name='search' />
          </NavItem>
          <NavItem>
            {present === true? <Button color='light' onClick=''>Sign in</Button> : null}
          </NavItem>
          <NavItem>
            <btn color='primary' onClick=''>
              <span className='fa fa-shopping-cart fa-lg p-2'></span>
            </btn>
          </NavItem>
        </Nav>
        
      </Navbar>
    </>)
  };

  export default Header;
