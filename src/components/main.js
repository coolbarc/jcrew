import {useEffect} from 'react';
import { Switch, Route, withRouter, Redirect } from 'react-router-dom';
import {connect} from 'react-redux';
import Header from './header';
import Footer from './footer';
import { fetchProducts } from '../redux/actionCreator';
import Products from './products';
import ProductInfo from './productInfo';


const mapStateToProps = (state) => {
    return {
        cart: state.cart,
        products:state.products,
        category:state.category
    }
};

const mapDispatchToProps = () => (dispatch) => (
    {
        fetchProducts: ()=>dispatch(fetchProducts())
    }
);

const Main = props => {
    useEffect(()=>{
        props.fetchProducts();
    },[]);

    const productDetails = ({match}) => {
        return(
            <ProductInfo 
                product={props.products.product.filter((info)=> info.productId === match.params.infoId)[0]}
                isLoading={props.products.isLoading}
                errMess={props.products.errMess}
            />
        )
    }

    return (
        <>
            <Header />
                <Switch>
                    <Route exact path='/products' component={()=><Products products={props.products.product}/>} />
                    <Route exact path='/products/:infoId' component={productDetails} />
                    <Route exact path='/cart' component={()=><h4>in progress...</h4>} />
                    <Redirect to="/products" />
                </Switch>
            <Footer />
        </>
    );
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Main));