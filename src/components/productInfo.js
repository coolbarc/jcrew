import { Card, CardHeader, CardBody, CardImg, Breadcrumb, BreadcrumbItem, CardText } from "reactstrap";
import { Loading } from "../shared/Loading";
import { Link } from 'react-router-dom';
import { useState } from "react";





const ProductInfo = props => {
const [isHover, setHover]=useState(false)
const [swachImage, setSwatchImage]=useState('')
    if (props.isLoading) {
        return (
            <div className="container">
                <div className="row">
                    <Loading />
                </div>
            </div>
        );
    }
    else if (props.errMess) {
        return (
            <div className="container">
                <div className="row">
                    <h4>{props.errMess}</h4>
                </div>
            </div>
        );
    }
    else if (props.product != null){
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to='/products'>Products</Link></BreadcrumbItem>
                        <BreadcrumbItem active>details</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>Product Details</h3>
                        <hr />
                    </div>
                </div>
                <div className="row">
                    <Card className='col-12 col-md-6'>
                        <CardHeader>{props.product.apiLink.split('/')[props.product.apiLink.split('/').length - 2]}</CardHeader>
                        {isHover?
                        <CardImg   src={`https://www.jcrew.com/s7-img-facade/${props.product.productCode}_${swachImage}`} /> 
                        :
                        <CardImg src={
                            `https://www.jcrew.com/s7-img-facade/${props.product.productCode}_${props.product.defaultColorCode}`} 
                            alt='picture' />}
                        <CardBody>
                            Price: {props.product.now.formatted}
                            <CardText><b>Description: </b>{props.product.productDescription}</CardText>
                            Available Colors: 
                            <div className='col-12'>
                            {props.product.colors.length > 0? 
                                props.product.colors.map(colr=>{
                                    return <div key={colr} className='d-inline-block mx-1'>
                                        <CardImg onMouseEnter={()=>{setHover(!isHover); setSwatchImage(colr.colorCode)}}  onMouseLeave={()=>setHover(!isHover)}  
                                        style={{borderRadius: '50%', width: '30px'}} src={`https://www.jcrew.com/s7-img-facade/${props.product.productCode}_${colr.colorCode}_sw`} /> 
                                    </div>
                                })
                            :
                            null
                            }
                            </div>
                        </CardBody>
                    </Card>
                </div>
            </div>
        );}
    else
        return (
            <div>No Info!!</div>
        );
};

export default ProductInfo;