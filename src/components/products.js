
import { Card,CardBody,CardHeader, CardImg} from 'reactstrap';
import { Loading } from '../shared/Loading';
import { Link } from 'react-router-dom'


const MakeProducts = ({products, isLoading, errMess}) => {
    const cont = products? products.map((item, i) =>{
       return  <div key={i} className="col-12  col-md-2 col-xl-2 m-1 d-md-inline-block">
            {/** filter products by price and productCode */}{item.productCode && item.now? 
            <Card >
                <CardHeader>{item.apiLink.split('/')[item.apiLink.split('/').length - 2]}</CardHeader>
                    <Link to={`/products/${item.productId}`}>
                        <CardImg src={
                        `https://www.jcrew.com/s7-img-facade/${item.productCode}_${item.defaultColorCode}`} alt='picture' />
                    </Link>
                <CardBody>Price: {item.now.formatted}</CardBody>
            </Card>
            :
            null}
        </div>
    })
    :
    <h3>no data</h3>


    if (isLoading) {
        return (
            <div className="container">
                <div className="row">
                    <Loading />
                </div>
            </div>
        );
    }
    else if (errMess) {
        return (
            <div className="container">
                <div className="row">
                    <h4>{errMess}</h4>
                </div>
            </div>
        );
    }
    else
        return (
            <div className="container">
                <div className="row">
                    <div className="col-12"><br/>
                        <h3>Product Page</h3>
                        <hr />
                    </div>
                </div>
                <div className="row ">
                    <div className="col-12 text-align-center m-2">{cont}</div>
                </div>
            </div>
        );

}

const Products = props => {

    return (<>
        <div className="col-12 mt-1 mb-1">
            <MakeProducts products={props.products} isLoading={props.isLoading} errMess={props.errMess}/>
        </div>
    </>)
};

export default Products;