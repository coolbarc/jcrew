import * as ActionTypes from './actionTypes';
import * as data from '../shared/data'


export const fetchProducts = () => (dispatch) => {
    const dat = data.data.productList[0].products
    dispatch(addProducts(dat))
}
export const productsLoading = () => ({
    type: ActionTypes.PRODUCT_LOADING
});

export const productsFailed = (errmess) => ({
    type: ActionTypes.PRODUCT_FAILED,
    payload: errmess
});

export const addProducts = (dishes) => ({
    type: ActionTypes.ADD_PRODUCT,
    payload: dishes
});