"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
exports.CART = void 0;
var ActionTypes = require("./actionTypes");
var CART = function (state, action) {
    if (state === void 0) { state = {
        isLoading: true,
        errMess: false,
        cart: []
    }; }
    switch (action.type) {
        case ActionTypes.ADD_CART:
            return __assign(__assign({}, state), { isLoading: false, errMess: null, cart: action.payload });
        case ActionTypes.CART_FAILED:
            return __assign(__assign({}, state), { isLoading: false, errMess: action.payload, cart: null });
        case ActionTypes.CART_LOADING:
            return __assign(__assign({}, state), { isLoading: true, errMess: null, cart: null });
        default:
            return state;
    }
};
exports.CART = CART;
