import * as ActionTypes from './actionTypes';

export const CART = (state = {
    isLoading: true,
    errMess: false,
    cart: []
}, action: { type: string; payload: string | []; }) => {
    switch (action.type){
        case ActionTypes.ADD_CART:
            return {...state, isLoading: false, errMess:null, cart: action.payload}
        case ActionTypes.CART_FAILED:
            return {...state, isLoading: false, errMess: action.payload, cart: null}
        case ActionTypes.CART_LOADING:
            return {...state, isLoading: true, errMess: null, cart: null}
        default:
            return state;
    }
};