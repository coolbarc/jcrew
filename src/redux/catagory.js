"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
exports.CATEGORY = void 0;
var ActionTypes = require("./actionTypes");
var CATEGORY = function (state, action) {
    if (state === void 0) { state = {
        isLoading: true,
        errMess: false,
        category: []
    }; }
    switch (action.type) {
        case ActionTypes.ADD_CATEGORY:
            return __assign(__assign({}, state), { isLoading: false, errMess: null, category: action.payload });
        case ActionTypes.CATEGORY_FAILED:
            return __assign(__assign({}, state), { isLoading: false, errMess: action.payload, category: null });
        case ActionTypes.CATEGORY_LOADING:
            return __assign(__assign({}, state), { isLoading: true, errMess: null, category: null });
        default:
            return state;
    }
};
exports.CATEGORY = CATEGORY;
