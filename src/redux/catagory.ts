import * as ActionTypes from './actionTypes';

export const CATEGORY = (state = {
    isLoading: true,
    errMess: false,
    category: []
}, action: { type: string; payload: string | []; }) => {
    switch (action.type){
        case ActionTypes.ADD_CATEGORY:
            return {...state, isLoading: false, errMess:null, category: action.payload}
        case ActionTypes.CATEGORY_FAILED:
            return {...state, isLoading: false, errMess: action.payload, category: null}
        case ActionTypes.CATEGORY_LOADING:
            return {...state, isLoading: true, errMess: null, category: null}
        default:
            return state;
    }
};