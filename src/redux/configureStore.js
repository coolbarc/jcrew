"use strict";
exports.__esModule = true;
exports.configureStore = void 0;
var redux_1 = require("redux");
var redux_thunk_1 = require("redux-thunk");
var redux_logger_1 = require("redux-logger");
var cart_1 = require("./cart");
var product_1 = require("./product");
var catagory_js_1 = require("./catagory.js");
var configureStore = function () {
    var store = redux_1.createStore(redux_1.combineReducers({
        cart: cart_1.CART,
        products: product_1.PRODUCTS,
        category: catagory_js_1.CATEGORY
    }), redux_1.applyMiddleware(redux_thunk_1["default"], redux_logger_1["default"]));
    return store;
};
exports.configureStore = configureStore;
