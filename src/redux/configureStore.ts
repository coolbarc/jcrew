import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import {CART} from './cart'
import {PRODUCTS} from './product';
import {CATEGORY} from './catagory.js';




export const configureStore = () => {
    const store = createStore(
        combineReducers(
            {
                cart: CART,
                products: PRODUCTS,
                category: CATEGORY
            }
        ),
        applyMiddleware(thunk, logger)
    );
    return store;
}