"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
exports.PRODUCTS = void 0;
var ActionTypes = require("./actionTypes");
var PRODUCTS = function (state, action) {
    if (state === void 0) { state = {
        isLoading: true,
        errMess: false,
        product: []
    }; }
    switch (action.type) {
        case ActionTypes.ADD_PRODUCT:
            return __assign(__assign({}, state), { isLoading: false, errMess: null, product: action.payload });
        case ActionTypes.PRODUCT_FAILED:
            return __assign(__assign({}, state), { isLoading: false, errMess: action.payload, product: null });
        case ActionTypes.PRODUCT_LOADING:
            return __assign(__assign({}, state), { isLoading: true, errMess: null, product: null });
        default:
            return state;
    }
};
exports.PRODUCTS = PRODUCTS;
