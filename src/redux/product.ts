import * as ActionTypes from './actionTypes';

export const PRODUCTS = (state = {
    isLoading: true,
    errMess: false,
    product: []
}, action: { type: string; payload: string | []; }) => {
    switch (action.type){
        case ActionTypes.ADD_PRODUCT:
            return {...state, isLoading: false, errMess:null, product: action.payload}
        case ActionTypes.PRODUCT_FAILED:
            return {...state, isLoading: false, errMess: action.payload, product: null}
        case ActionTypes.PRODUCT_LOADING:
            return {...state, isLoading: true, errMess: null, product: null}
        default:
            return state;
    }
};