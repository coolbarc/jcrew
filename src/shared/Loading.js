import React from 'react';

export const Loading = () => {
    return(
        <div className='col-12'>
            <span className="fa fa-spinner-grow fa-pulse fa-3x fa-fw text-light"></span>
            <span className="fa fa-spinner fa-pulse fa-3x fa-fw text-primary"></span>
            <span className="fa fa-spinner fa-pulse fa-3x fa-fw text-info"></span>
            <span className="fa fa-spinner fa-pulse fa-3x fa-fw text-danger"></span>
        </div>
    )
}